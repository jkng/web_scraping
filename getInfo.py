# This file gets all the links inside the given link with their title
# Site choosen is 'https://uncareer.net/organization'
# Oraganaization choosen is 'Abt Associates'

import requests
from bs4 import BeautifulSoup
import csv

# Setting p te sorce
source_2 = requests.get(
    'https://uncareer.net/vacancy/director-international-accounting-185587')


soup_2 = BeautifulSoup(source_2.text, 'html.parser')

body_2 = soup_2.find('body')

vacancy_info = body_2.find(class_='vacancy-text')

print(vacancy_info.prettify())
