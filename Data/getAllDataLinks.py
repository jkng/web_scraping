from bs4 import BeautifulSoup
import csv
import requests


page = requests.get(
    'https://uncareer.net/organization')

# Create a BeautifulSoup object
soup = BeautifulSoup(page.text, 'html.parser')

f = csv.writer(open('organaization-names.csv', 'w', encoding='utf8'))
f.writerow(['Name', 'Link'])


# Pull all text from the BodyText div
artist_name_list = soup.find(class_='columns')
list = artist_name_list.find_all(class_='column-element')


def getData(name, link, count):
    source = requests.get(link)

    f = csv.writer(open('Org-' + str(count) + '.csv', 'w', encoding='utf8'))
    f.writerow([name])
    f.writerow(['S.No', 'Name', 'Link'])

    soup = BeautifulSoup(source.text, 'html.parser')
    body = soup.find('body')
    columns = body.find(class_='columns')
    vacancy = columns.find_all(class_='vacancy')

    count = 1
    for item in vacancy:
        name_1 = item.a.contents[0]
        link_1 = 'https://uncareer.net' + item.a.get('href')
        # print(str(count) + ') ' + name_1 + ': ' + link_1)
        f.writerow([count, name_1, link_1])
        count = count + 1


count_1 = 1
for items in list:
    name = items.a.contents[0]
    link = 'https://uncareer.net' + items.a.get('href')
    f.writerow([name, link])
    count_1 = count_1 + 1
    getData(name, link, count_1)
