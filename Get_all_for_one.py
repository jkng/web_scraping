# This file gets all the links inside the given link with their title
# Site choosen is 'https://uncareer.net/organization'
# Oraganaization choosen is 'Abt Associates'

import requests
from bs4 import BeautifulSoup
import csv

source = requests.get(
    'https://uncareer.net/organization/Abt+Associates')

# Opening/Creating a file
f = csv.writer(open('Abt-Associates.csv', 'w', encoding='utf8'))
f.writerow(['Abt Associates'])
f.writerow(['S.No', 'Name', 'Link'])


soup = BeautifulSoup(source.text, 'html.parser')

body = soup.find('body')

columns = body.find(class_='columns')

vacancy = columns.find_all(class_='vacancy')

count = 1

# Printing all the opportunities (name and link)
# A new Abt-Associates.csv file will be created in te same directory
# Results will printed in terinal as well as in the file
for item in vacancy:
    name = item.a.contents[0]
    link = 'https://uncareer.net' + item.a.get('href')
    print(str(count) + ') ' + name + ': ' + link)
    f.writerow([count, name, link])
    count = count + 1
