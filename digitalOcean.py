from bs4 import BeautifulSoup
import csv
import requests

# Setting up the source page
page = requests.get(
    'https://uncareer.net/organization')

# Create a BeautifulSoup object and assigning it a html parser
soup = BeautifulSoup(page.text, 'html.parser')

# Open/Create a file
f = csv.writer(open('organaization-names.csv', 'w', encoding='utf8'))
f.writerow(['Name', 'Link'])


# Pull all text from the BodyText div
artist_name_list = soup.find(class_='columns')
# Finding all the div with name column-element (Why...?  Inspect the names of organaization)
list = artist_name_list.find_all(class_='column-element')

# Extract title and link for each Organaization
for items in list:
    name = items.a.contents[0]
    links = 'https://uncareer.net' + items.a.get('href')
    f.writerow([name, links])



